// Import de la class permettant de créer un composant
import { Component, OnInit } from '@angular/core';
import { MenuController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { WordpressService } from '../../services/wordpress.service';

// Décorateur du composant
@Component({
  selector: 'home-page',
  templateUrl: 'app.homepage.html'
})


// Export de la class du composant
export class AppHomepage implements OnInit {
  request = {};
  access: any;
  errorMessage: any;

  openMenu() {
    // Ouverture du burger menu
    this.menu.open();
  }

  login(){
    this.wordpressService.getUserRequest();
  }
  
  constructor(
    public menu: MenuController,
    private wordpressService: WordpressService,
    private http: Http
  ){}

  ngOnInit(){
    // this.wordpressService.getUserAccess().then( data => console.log(data) )
  }

 }
