import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from 'ionic-angular';

// Import du service
import { WordpressService } from '../../services/wordpress.service';

// Import du composant de la vue memo-modal pour l'injecter dans la modale
import { MemoModal } from '../memo-modal/memo.modal';






// Décorateur du composant MemoList
@Component({
  // Définition du selecteur, nécessaire pour les routes
  selector: 'memo-list',
  templateUrl: 'memo.list.html',

  // Définition du service dans le tableau des providers
  providers: [ WordpressService ]
})






// Export de la class du composant
export class MemoList implements OnInit   {
  // Définition des variables
  supportId: any;
  selectedItems: any;
  selectedSlug: any;
  title: string;
  errorMessage: string;
  collection: any[];

  // Création d'une fonction pour ouvrir une modal -> https://goo.gl/fk3mMT
  openModal(item) {
    // Création de la modal
    let modal = this.modalCtrl.create(MemoModal, item);
    // Ouverture de la modal
    modal.present();
  }

  // Création d'une fonction pour afficher le titre de la page depuis le service
  getMemoTitle(slug :any){
    this.wordpressService.getMemoTitle(slug, 'http://www.juliennoyer.com/wp-json/wp/v2/categories?per_page=100&orderby=id&parent=' + this.supportId).then(data => this.title = data);
  }
  
  // Création d'un fonction pour afficher la liste de items mémo depuis le service
  getMemoContent(itemId :number){
    this.wordpressService.getMemoList(itemId).then( data => this.collection = data, error =>  this.errorMessage = <any>error);
  }

  // Utilisation de la fonction ngOnInit -> https://goo.gl/JxoTRo
  ngOnInit() { 
    this.getMemoTitle(this.selectedSlug );
    this.getMemoContent(this.selectedItems );
    
  }

  // Constructeur du composant
  constructor(
    // Ajout des composants pour gérer la navigation
    public navParams: NavParams,
    private wordpressService: WordpressService,

    // Ajout du composant pour gérer les modals -> https://goo.gl/fk3mMT
    public modalCtrl: ModalController
    ) {
    
    // Définition de la catégorie de mémo à afficher en récupérant le paramètre item de la route
    this.selectedItems = navParams.get('itemId');
    this.selectedSlug = navParams.get('item');
    this.supportId = navParams.get('supportId');

  }
}
