import { Component } from '@angular/core';

// Import des composant pour gérer les modals, la navigation et les vues
import { Platform, NavParams, ViewController } from 'ionic-angular';






// Décorateur du composant de la modal
@Component({
  templateUrl: 'memo.modal.html',
})






// Export de la class du composant
export class MemoModal {
  // Définition des variables
  itemDetail;

  // Création d'une fonction pour fermer la modal'
  dismiss() {
    this.viewCtrl.dismiss();
  }

  // Constructeur du composant
  constructor(
    // Ajout des composants pour gérer la plateforme -> https://goo.gl/r03y2g
    public platform: Platform,
    // Ajout des composants pour gérer la navigation
    public params: NavParams,
    // Ajout des composants pour gérer la vue
    public viewCtrl: ViewController
  ) {
    
    // Récupération des données de l'item grâce au paramètre de la route
    this.itemDetail = this.params.get('selected');
  }
}