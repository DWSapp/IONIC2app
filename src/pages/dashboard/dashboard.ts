import { Component, OnInit } from '@angular/core';

// Import des composant pour gérer la navigation
import { NavController, NavParams } from 'ionic-angular';

// Import du service
import { WordpressService } from '../../services/wordpress.service';

// Import du composant de la vu memo-list pour l'injecter dans la vue
import { MemoList } from '../memo-list/memo.list';






// Décorateur du composant AppDashboard
@Component({
  // Définition du selecteur, nécessaire pour les routes
  selector: 'dashboard-page',
  templateUrl: 'dashboard.html',

  // Définition du service dans le tableau des providers
  providers: [ WordpressService ]
})






// Export de la class du composant
export class AppDashboard implements OnInit  {

  // Définition des variables
  errorMessage: string;
  wpCollection: any[];
  supportTitle: string;
  pageQuote: string;
  supportId: number;

  // Création d'une fonction pour afficher la collection de données
  getMemosFromWordpress() {
    this.wordpressService.getCategoryList('http://www.juliennoyer.com/wp-json/wp/v2/categories?orderby=id&parent=' + this.supportId)
    .then( data => this.wpCollection = data, error =>  this.errorMessage = <any>error);
  }

  // Création d'un fonction pour définir la route d'un item
  getWpMemoItem(memoItem: any): void {
    // Injection du composant memo-list dans la vue -> https://goo.gl/1Dl5p7
    this.navCtrl.push(MemoList, 
    // Ajout d'un objet en paramètre de la fonction pour afficher le contenu du slug
    { item: memoItem.slug, itemId: memoItem.id, supportId: this.supportId });;
  };


  // Utilisation de la fonction ngOnInit -> https://goo.gl/JxoTRo
  ngOnInit() { 
    // Appel de la fonction pour afficher la collection de données
    this.getMemosFromWordpress();
  };


  // Constructeur du composant
  constructor(
    // Ajout des composants pour gérer la navigation
    public navCtrl: NavController, 
    public navParams: NavParams,

    // Ajout du service dans le constructor
    private wordpressService: WordpressService,


  ) { 
      this.supportId = this.navParams.data.supportId; 
      this.supportTitle = this.navParams.data.title;
      this.pageQuote = this.navParams.data.quote;
    };
}
