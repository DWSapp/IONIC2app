import { Component, OnInit } from '@angular/core';

// Import des composant pour gérer la navigation
import { NavController, NavParams } from 'ionic-angular';

// Import du service
import { WordpressService } from '../../services/wordpress.service';


// Décorateur du composant AppDashboard
@Component({
  // Définition du selecteur, nécessaire pour les routes
  selector: 'contributors-page',
  templateUrl: 'contributors.html',

  // Définition du service dans le tableau des providers
  providers: [ WordpressService ]
})






// Export de la class du composant
export class ContributorsComponent implements OnInit  {

  // Définition des variables
  errorMessage: string;
  wpCollection: any[];
  pageQuote: string;

  // Création d'une fonction pour afficher la collection de données
  getContributorsFromWordpress() {
    this.wordpressService.getContributors()
    .then( data => this.wpCollection = data, error =>  this.errorMessage = <any>error);
  }

  // Utilisation de la fonction ngOnInit -> https://goo.gl/JxoTRo
  ngOnInit() { 
    // Appel de la fonction pour afficher la collection de données
    this.getContributorsFromWordpress(); 
  };



  // Constructeur du composant
  constructor(
    // Ajout des composants pour gérer la navigation
    public navCtrl: NavController, 
    public navParams: NavParams,

    // Ajout du service dans le constructor
    private wordpressService: WordpressService

  ) { 
    this.pageQuote = this.navParams.data.quote;
  };
}
