"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var http_2 = require('@angular/http');
var WordpressService = (function () {
    function WordpressService(http) {
        this.http = http;
        this.myHeader = new http_2.Headers({ 'Authorization': '"Basic" base64_encode( "DWSjul":"TirxRZhtv7DscdWwURJi92eydskIaz" )' });
        this.getCategoriesUrl = 'http://www.juliennoyer.com/wp-json/wp/v2/categories?orderby=id&parent=13';
        this.getMemoListUrl = 'http://www.juliennoyer.com/wp-json/wp/v2/posts?order=asc&filter[category_name]=';
        this.getFooterListUrl = 'http://www.juliennoyer.com/wp-json/wp/v2/posts/162';
    }
    // Création d'une fonction pour charger la liste des catégories Memo de l'API WP
    WordpressService.prototype.getCategoryList = function () {
        return this.http.get(this.getCategoriesUrl, { headers: new http_2.Headers(this.myHeader) })
            .toPromise().then(this.getDataFromWordpress).catch(this.handleError);
    };
    // Création d'une fonction pour charge les posts d'une catégorie de l'API WP
    WordpressService.prototype.getMemoTitle = function (slug) {
        return this.getCategoryList().then(function (memos) { return memos.find(function (title) { return title.slug === slug; }); });
    };
    // Création d'une fonction pour charge les posts d'une catégorie de l'API WP
    WordpressService.prototype.getMemoList = function (slug) {
        return this.http.get(this.getMemoListUrl + slug)
            .toPromise().then(this.getDataFromWordpress).catch(this.handleError);
    };
    // Création d'un fonction .get() sur un fichier json utilisant le système de promise
    WordpressService.prototype.getFooterList = function () {
        return this.http.get(this.getFooterListUrl).toPromise().then(this.getDataFromWordpress).catch(this.handleError);
    };
    // Traitement de la réponse json
    WordpressService.prototype.getDataFromWordpress = function (res) {
        return res.json() || {};
    };
    // Traitement des erreurs
    WordpressService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg);
        return Promise.reject(errMsg);
    };
    WordpressService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], WordpressService);
    return WordpressService;
}());
exports.WordpressService = WordpressService;
//# sourceMappingURL=wordpress.service.js.map