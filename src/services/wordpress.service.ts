import { Injectable }     from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class WordpressService {

  private getMemoListUrl = 'http://juliennoyer.com/wp-json/wp/v2/posts?per_page=100&categories=';
  private getContributorsUrl = 'http://www.juliennoyer.com/wp-json/wp/v2/users';
  private getFooterListUrl = 'http://www.juliennoyer.com/wp-json/wp/v2/posts/162';

  constructor ( private http: Http ) {}


  getUserRequest() {
    var apiHost = 'http://juliennoyer.com/wp-json';

    return this.http.post( apiHost + '/jwt-auth/v1/token', {
        username: 'MRrobot',
        password: '0tR3rDkarfVSj@z&ge(i5k^f'
      } )

      .toPromise()
      .then( function( response ) {
        console.log( response )
      } )

      .catch( function( error ) {
        console.error( 'Error', error.data[0] );
      } );
  }

  // Création d'une fonction pour charger la liste des catégories Memo de l'API WP
  getCategoryList (link): Promise<any[]> {
    return this.http.get(link)
    .toPromise().then(this.getDataFromWordpress).catch(this.handleError);
  }

  // Création d'une fonction pour récupérer le titre de la catégorie affichée
  getMemoTitle(slug: any, link): Promise<any> {
      return this.getCategoryList(link).then(memos => memos.find(title => title.slug === slug));
  }

  // Création d'une fonction pour charge les posts d'une catégorie de l'API WP
  getMemoList (slug: any): Promise<any[]> {
    return this.http.get(this.getMemoListUrl + slug)
    .toPromise().then(this.getDataFromWordpress).catch(this.handleError);
  }

  // Création d'un fonction pour charger la liste des contributeurs
  getContributors (): Promise<any[]> {
    return this.http.get(this.getContributorsUrl)
    .toPromise().then(this.getDataFromWordpress).catch(this.handleError);
  }
  
  // Création d'un fonction .get() sur un fichier json utilisant le système de promise
  getFooterList (): Promise<any[]> {
    return this.http.get(this.getFooterListUrl).toPromise().then(this.getDataFromWordpress).catch(this.handleError);
  }

  // Traitement de la réponse json
  private getDataFromWordpress(res: Response) {
    return res.json() || { };
  }

  // Traitement des erreurs
  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Promise.reject(errMsg);
  }
}