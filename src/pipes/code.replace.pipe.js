"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var CodeReplacePipe = (function () {
    function CodeReplacePipe() {
    }
    CodeReplacePipe.prototype.transform = function (value, args) {
        if (!value)
            return value;
        return value.replace(/\</g, '&lt;').replace(/\>/g, '&gt;');
    };
    CodeReplacePipe = __decorate([
        core_1.Pipe({ name: 'codeReplace' }), 
        __metadata('design:paramtypes', [])
    ], CodeReplacePipe);
    return CodeReplacePipe;
}());
exports.CodeReplacePipe = CodeReplacePipe;
//# sourceMappingURL=code.replace.pipe.js.map