import { Pipe, PipeTransform } from '@angular/core';



// Décorateur du pipe CodeReplacePipe
@Pipe({name: 'codeReplace'})



// Export de la class du pipe
export class CodeReplacePipe implements PipeTransform {

  // Création d'un filtre pour afficher correctment des accolade "<" et ">"
  transform(value: string): any {
    if (!value) return value;
    return value.replace(/\&lt;/g, '<')
                .replace(/\&gt;/g, '>')
                .replace(/\&#8217;/g, "'")
                .replace(/\&#039;/g, "'")
                .replace(/\&quot;/g, '"')
                .replace(/\&#8211;/g, '-')
  }
  
}