import { NgModule, ErrorHandler } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

// Import des composants de l'application
import { MyApp } from './app.component';
import { AppHomepage } from '../pages/app-homepage/app.homepage';
import { AppDashboard } from '../pages/dashboard/dashboard';
import { MemoList } from '../pages/memo-list/memo.list';
import { MemoModal } from '../pages/memo-modal/memo.modal';
import { ContributorsComponent } from '../pages/contributors/contributors';

// Import du service
import { WordpressService } from '../services/wordpress.service';

// Import du pipe
import { CodeReplacePipe } from '../pipes/code.replace.pipe';




// Décorateur du module de l'application
@NgModule({

  declarations: [
    // Déclaration des composants
    MyApp,
    AppHomepage,
    AppDashboard,
    MemoList,
    MemoModal,
    ContributorsComponent,

    // Déclaration du pipe
    CodeReplacePipe
  ],

  imports: [
    IonicModule.forRoot(MyApp, {
      // Définition des constantes natives
      backButtonText: 'Retour'
     })],

  bootstrap: [IonicApp],

  entryComponents: [
    // Déclaration des composants utilisés dans le systeme de navigation -> https://goo.gl/MYmXj1
    MyApp,
    AppHomepage,
    AppDashboard,
    MemoList,
    MemoModal,
    ContributorsComponent
  ],

  providers: [
    // Déclaration du provider pour Ionic2
    { provide: ErrorHandler, useClass: IonicErrorHandler }, 

    // Déclaration du service
    WordpressService,

    Storage
    ]
})




// Export de la class du module de l'application
export class AppModule {}
