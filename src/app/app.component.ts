import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

// App Storage
import { Storage } from '@ionic/storage';

// Import des RXJS pour gérer les promesses 
import './rxjs-operators';

// Import des composants à utiliser dans le burger menu
import { AppHomepage } from '../pages/app-homepage/app.homepage';
import { AppDashboard } from '../pages/dashboard/dashboard';
import { ContributorsComponent } from '../pages/contributors/contributors';




// Décorateur du composant principal de l'application
@Component({
  templateUrl: 'app.html'
})




// Export de la class du composant principal
export class MyApp {
  // Intégration des fonctions du composant Nav (nécessaire pour l'utilisation du burger menu)
  @ViewChild(Nav) nav: Nav;
  // Définition de la vue principale de l'application : AppDashboard
  rootPage: any = AppHomepage;
  // Définition d'un variable pour afficher la liste des vues dans le burger menu
  pagesList: any;




  // Création d'une fonction pour initialiser l'application
  initializeApp() {
    // Appel de la fonction ready() du composant Platform
    this.platform.ready().then(() => {
      // Définition des fonctions native à utiliser dand l'application
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  // Création d'un fonction permettant de gérer la passage entre les vues
  openPage(page) {
    // Fermeture du burger menu
    this.menu.close();
    // Affichage de la vue sélectionnée
    this.nav.setRoot(page.component, page);
  }





  // Constructeur du composant prinsipal
  constructor(
    // Ajout des composants Platform et MenuController pour utilier le burger menu
    public platform: Platform,
    public menu: MenuController,

    // App Storage
    storage: Storage
  ) {


    // set a key/value
    storage.set('name', 'Max');

    // Or to get a key/value pair
    storage.get('name').then((val) => {
      // console.log('Your name is', val);
    })


    // Création de la liste des pages dans le burger menu
    this.pagesList = [
      { title: 'Accueil', component: AppHomepage},
      { title: 'Supports FrontEnd', quote: 'Best coding practices', component: AppDashboard, supportId: 18 },
      { title: 'Support AngularJS', quote: 'Become a WebApp developer', component: AppDashboard, supportId: 19 },
      { title: 'Contributeurs', quote: 'Behind the app', component: ContributorsComponent }
    ];

    // Initialisation de l'application
    this.initializeApp();
  }
}
